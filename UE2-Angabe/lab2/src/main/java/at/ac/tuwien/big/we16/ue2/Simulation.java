package at.ac.tuwien.big.we16.ue2;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TimerTask;

import at.ac.tuwien.big.we16.ue2.beans.AuctionBean;
import at.ac.tuwien.big.we16.ue2.beans.UserBean;
import at.ac.tuwien.big.we16.ue2.service.NotifierService;
import at.ac.tuwien.big.we16.ue2.service.ServiceFactory;

public class Simulation implements Runnable {

	private UserBean comp;
	private DataStore data;

	public Simulation(UserBean comp, DataStore data) {
		this.comp = comp;
		this.data = data;
	}

	@Override
	public void run() {
		Random random = new Random();

		// notifierservice aus der factory createn
		NotifierService service = ServiceFactory.getNotifierService();

		Set<AuctionBean> auctions = data.getAllAuctions();

		for (AuctionBean currentAuction : auctions) {

			int randNumber = random.nextInt(100);
			double bid = currentAuction.getBid() + 2.5;

			if (bid <= comp.getCredit() && randNumber < 30 && currentAuction.isBidden() && !currentAuction.isExpired()) {
				// der vorherige hoechtbieter
				UserBean preHighestUser = currentAuction.getHighestBidder();
				double preBid = currentAuction.getBid(); // vorheriges
															// hoechstgebot

				// dem alten hoechstbietenden muss sein gebot wieder zurueck
				// ueberwiesen werden
				// falls es einen gab bzw er sich nicht selber ueberbietet
				if (preHighestUser != null && preHighestUser != comp) {
					preHighestUser.setCredit(preHighestUser.getCredit() + preBid);
					// aufruf zum service zum websocket
					service.updatePreHighestBidderCredit(preHighestUser);
				}
					// liste der Mitbietenden aktualisieren (bzw erstellen wenn
					// es noch keine gebote gibt)
					List<UserBean> userList = data.getUsersfromAuction(currentAuction);
					if (!userList.contains(comp)) {
						userList.add(comp);
					}
					
					// usercredit aktualisieren (wenn er bereits
					// hoechstbietender ist, wird nur die differenz zu altem
					// gebot abgezogen
					if (currentAuction.getHighestBidder() != null && currentAuction.getHighestBidder().equals(comp)) {
						comp.setCredit(comp.getCredit() - bid + currentAuction.getBid());
					} else {
						comp.setCredit(comp.getCredit() - bid);
					}
					
					// Hoechstbietenden aktualisieren
					currentAuction.setHighestBidder(comp);
					// Hoechstgebot aktualisieren
					currentAuction.setBid(bid);

					//
					// alle anderen bieter benachrichtigen und ihre overview
					// page aktualisieren
					//
					service.sendHighestBidToBidders(currentAuction);
			}

		}
	}
}

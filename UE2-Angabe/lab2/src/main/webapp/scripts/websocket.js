/**
 * hier wird der websocket erstellt 
 */

var socket = new WebSocket("ws://localhost:8080/we16-ue2/socket");
socket.onmessage = function(event) {
	
	var msg = event.data;	
	var sentData = msg.split(";");
	
	// wenn eine auktion ueberboten wird werden alle benachrichtigt und richtig gehighlightet
	// sprich der hoechstbietende erhaelt ein highlight bei seiner auktion und der alte 
	// hoechstbietende verliert es 
	if(sentData[0] == "notifyAll") {
		console.log("geht in notfy aller user");
		//alert(sentData[1]);
        var product_name = document.getElementById("highest_bidder_" + sentData[1]);
        product_name.firstChild.nodeValue = sentData[2].split("@")[0]; // injection vom user namen
        
        var prod_price = document.getElementById("price_"+sentData[1]);
        prod_price.firstChild.nodeValue = (sentData[3]);  // injection des preises in overview
        
        // ueberpruefen ob die session vom highest bidder ist und bei ihm das highlight setzen
        var inputs = document.getElementsByTagName("dd"); // in einem dd steht die ID des users gespeichert
		
        // da bei dieser auction ein hoechstgebot neu vergeben wurde verlieren alle das highlight
        // wenn sie keins hatten passiert nichts. die schleife sucht dann den hoechstbieter und highlightet 
        // seine neue auktion
        $("#product_"+sentData[1]).removeClass("highlight");
        
        // alle dd tags durchiterieren um den tag mit der userid zu finden
    	for(var i= 0; i < inputs.length; i++) {
        	if(inputs[i].id == ("user_"+sentData[4])) {
        		// beim user der gerade das hoechstgebot gesetzt hat wird gehighlightet
        		$("#product_"+sentData[1]).addClass("highlight"); 
        	}
        }
  
	}
	
	// wenn eine auktion ablaeuft werden die daten laufende/gewonnene/verlorene auktion geudatet
	if(sentData[0] == "expired") {
		var running = document.getElementById("running_user_"+sentData[2]);
		running.firstChild.nodeValue = sentData[3];
		
		var won = document.getElementById("won_user_"+sentData[2]);
		won.firstChild.nodeValue = sentData[4];
		
		var lost = document.getElementById("lost_user_"+sentData[2]);
		lost.firstChild.nodeValue = sentData[5];
		
		
        // den tag finden wo die id des users gespeichert ist 
        var inputs = document.getElementsByTagName("dd"); // in einem dd steht die ID des users gespeichert
		var userid = sentData[1];
		var newCreditValue = sentData[6];
        // alle dd tags durchiterieren um den tag mit der userid zu finden
    	for(var i= 0; i < inputs.length; i++) {
        	if(inputs[i].id == ("user_"+ userid)) {
        		var cred = document.getElementById("user_balance");
        		cred.firstChild.nodeValue = newCreditValue;
        	}
        }
	}
	
	
	// wenn ein user uberboten wurde wird sein kontostand zurueckgesetzt
	if(sentData[0] == "updateCredit") {
		
		var userid = sentData[1];
		var newCreditValue = sentData[2];
		
        // den tag finden wo die id des users gespeichert ist 
        var inputs = document.getElementsByTagName("dd"); // in einem dd steht die ID des users gespeichert
		
        // alle dd tags durchiterieren um den tag mit der userid zu finden
    	for(var i= 0; i < inputs.length; i++) {
        	if(inputs[i].id == ("user_"+ userid)) {
        		var cred = document.getElementById("user_balance");
        		cred.firstChild.nodeValue = newCreditValue;
        	}
        }	
	}
};






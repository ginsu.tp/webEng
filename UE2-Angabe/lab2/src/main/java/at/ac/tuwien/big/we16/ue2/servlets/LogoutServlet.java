package at.ac.tuwien.big.we16.ue2.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by ASUSG74 on 20.04.16.
 */
@WebServlet (name="Logout", urlPatterns = {"/Logout"})
public class LogoutServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        session.invalidate();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/login.jsp");
        dispatcher.forward(request, response);
    }
}

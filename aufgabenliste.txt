lt MVC 2 Web Applikationen 
Trennung von Logik(eigene API); UI(JSP); Spielflusskontrolle(Servlet); Daten(Beans)

//HTML code in JSP auslagern um code zu sparen

 
Anforderungen:
SERVLET [
1. bei erstaufruf soll Anmeldeformular angezeigt werden. (keine �berpr�fung der Daten)
Nach anmeldung werden alle Produkte in einer Overview angezeigt.
 
2. Klick auf ein Produkt -> Detailansicht der auktion / wenn Auktion noch nicht 
abgelaufen ist soll ein Gebot abgegeben werden k�nnen. 

3. in Overview soll der H�chstbietende mittels .highlight(CSS) mit organgengen
Rahmen angezeigt werden. 

4. jede produkt hat eine zeitspanne f�r die auktion. wenn die um ist hat der
h�chbietende gewonnen. 

5. Abgelaufene Auktionen sollen in der �bersicht die klassen .expired erhalten. 
diese werden dann ausgegraut angezeigt. au0erdem soll in der detailansicht
anstelle des Bieten Formulas ein Text ausgegeben werden wo drinn steht
wer das produkt zu welchem preis ersteigert hat. 
]

BIETEN: (hoche zusammenarbeit mit WEBSOCKET)
6. kaufen von guthaben ist nicht teil der �bung, es soll jeder Benutzer beim 
Start ein Guthaben von 1500� haben


7. Gebote k�nnen nur �ber die Bieten funktion auf der detailseite gehen. 
Das formular wird �ber AJAX geschickt. Das angebot wird dann am server 
validiert (ob es h�her als das aktuelle angebot ist) => wenn nicht h�her
kommt eine fehlermeldung auf der detailseite / wenn positiv dann wird der kontostand
des users und die auktion aktualisiert und mit dem neuen gebot am server gespeichert.

8. der z�hler der laufenden auktionen wird erst bei der ersten teilname erh�ht und am
ende einer auktion  verringert. danach wird der z�hler der gewonnen und verlorenen
auktionen erh�ht. 

9. Die Daten (Anzahl laufender Auktionen, Kontostand) werden als response auf den 
ajax request geschickt an den Client(aktualisiert Detail Seite). Au�erdem 
werden allen anderen Clients die der Name und den Betrag des aktuellen h�chstbietenden
geschickt. au�erdem wird das vorher geltende Gebot dem alten Bieter wieder gutgeschrieben
werden(am server). 


COOMPUTERBENUTZER
10. zur simulation soll mindestens ein Comupterbenutzer in 20 Sekunden 
auf jedes Produkt mit einer wahrscheinlichkeit von 30% das aktuelle gebot �berbieten

WEBSOCKET
um daten akutlell zu halben soll server die informationen �ber gebote und abgelaufene
auktionen mittels websocket an verbundene clients senden
daten die �bertragen werden sollen: 

11. (ABH�NGIG VOM EMPF�ANGER) wenn auktion abgelaufen ist wird allen angemeldeten usern mitgeteilt 
welche auktion abgelaufen ist und was der aktuelle kontostand ist, wie viele
auktionen gerade laufen bzw verloren oder gewonnen wurden. Mit hilfe dieser daten
wird die abgelaufeene auktoon auf der �bersichtseite als solche markiert (css .expired)
dann wird das bieten formular der detailseite durch ein informationstext ersetzt
nd die sidebar aktualisiert. 

12. (F�R ALLE CLIENTS)wenn ein valides gebot von einem user gesendet wird, werden alle 
angemeldeten usern eine nachricht gesendet, welche produkt ID, der name
des neuen h�chstbietenden und den betrag. mit hilfe der daten werden
die angezeigten informationen in der �bersicht und auf der detailseite aktualisiert.

13. (ABH�NGIG VOM EMPF�ANGER)wenn ein benutzer �berboten wird, dann wird ihm
via websocket mitgeteilt wie hoch der konstostand nach der erfolgten gutschrift
ist damit der alte kontostand der sidebar ersettzt werden kann. 

parallele benutzung der plattform
14. selben server mehrere benutzer gleichzeigt. mehrer auktionen gleichzeitig
testen mit unterschiedlichen browsern gleichzeitig TESTING, nicht mehrere
tabs im selben browser zum testen verwenden

dynamische inhalte
15. produktdaten und informationen �ber die benutzer zb pname, akt preis, h�chstbietender
kontostand, anzahl der laufenden/gewonnenen/ auktionen DYNAMISCH werden.

local storage
16. html5 merkt sich im local storage was zuletzt angesehen wurde. wenn der browser
das nicht unters�tzt soll framework.js

barrierefreiheit
17.user interface muss anforderungen von HTML5 wie WCAG-AA gerecht sein.

speichern von daten
18. alle daten sind in java variablen gespeichert. => wenn applikation
geschlossen wird, sind die daten weg (UNN�TIG)

19. keine pw �berpr�fung (UNN�TIG)






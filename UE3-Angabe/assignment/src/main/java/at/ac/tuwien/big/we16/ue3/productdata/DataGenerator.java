package at.ac.tuwien.big.we16.ue3.productdata;

import at.ac.tuwien.big.we.dbpedia.api.DBPediaService;
import at.ac.tuwien.big.we.dbpedia.api.SelectQueryBuilder;
import at.ac.tuwien.big.we.dbpedia.vocabulary.DBPedia;
import at.ac.tuwien.big.we.dbpedia.vocabulary.DBPediaOWL;
import at.ac.tuwien.big.we16.ue3.model.Product;
import at.ac.tuwien.big.we16.ue3.model.ProductType;
import at.ac.tuwien.big.we16.ue3.model.RelatedProduct;
import at.ac.tuwien.big.we16.ue3.model.User;
import at.ac.tuwien.big.we16.ue3.service.ServiceFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

public class DataGenerator {

    private static java.util.logging.Logger logger = java.util.logging.Logger.getLogger(DataGenerator.class.getName());
    private EntityManager entityManager;
    private ServiceFactory serviceFactory;

    public void generateData() {
        logger.info("Generate data is started.");
        entityManager = ServiceFactory.getEntityManagerFactory().createEntityManager();
        generateUserData();
        generateProductData();
        entityManager.close();
    }

    private void generateUserData() {
        // TODO add the computer user to the database
        logger.info("Generated userdata is started.");
        User user = new User();
        user.setEmail("example@example.com");
        user.setPassword("Password");
        user.setFirstname("Test");
        user.setLastname("User");
        
        user.setDate(Date.from(LocalDate.of(1990, 01, 01).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
        user.setBalance(1500000);
        user.setRunningAuctionsCount(0);
        user.setWonAuctionsCount(0);
        user.setLostAuctionsCount(0);
        serviceFactory.getUserService().createUser(user);
        logger.info("Generate data is finished.");
    }

    private void generateProductData() {
        // TODO load products via JSONDataLoader and write them to the database
        logger.info("Generated productdata is started.");
        logger.info("Generated bookdata is started.");
        for (JSONDataLoader.Book book : JSONDataLoader.getBooks()) {
            Product p = new Product();
            p.setName(book.getTitle());
            p.setImage(book.getImg());
            p.setImageAlt("Image_" + convertBlanksToUnderline(book.getTitle()));
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_WEEK, 2);
            p.setAuctionEnd(calendar.getTime());
            p.setType(ProductType.BOOK);
            p.setYear(Integer.parseInt(book.getYear()));
            p.setProducer(book.getAuthor());
            p.setExpired(false);
            logger.info("Book: " + p.getName() + " created!");
            serviceFactory.getProductService().createProduct(p);
            p.setRelatedProducts(createDBPedia(p.getProducer(), DBPediaOWL.Book, DBPediaOWL.author , Locale.GERMAN,p));
        }
        logger.info("Generated bookdata is finished.");

        logger.info("Generated moviedata is started.");
        for (JSONDataLoader.Movie movie : JSONDataLoader.getFilms()) {
            Product p = new Product();
            p.setName(movie.getTitle());
            p.setImage(movie.getImg());
            p.setImageAlt("Image_" + convertBlanksToUnderline(movie.getTitle()));
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_WEEK, 2);
            p.setAuctionEnd(calendar.getTime());
            p.setType(ProductType.FILM);
            p.setYear(Integer.parseInt(movie.getYear()));
            p.setProducer(movie.getDirector());
            p.setExpired(false);
            logger.info("Movie: " + p.getName() + " created!");
            serviceFactory.getProductService().createProduct(p);
            p.setRelatedProducts(createDBPedia(p.getProducer(), DBPediaOWL.Film, DBPediaOWL.director, Locale.GERMAN,p));
        }
        logger.info("Generated moviedata is finished.");

        logger.info("Generated musicdata is started.");
        for (JSONDataLoader.Music music : JSONDataLoader.getMusic()) {
            Product p = new Product();
            p.setName(music.getAlbum_name());
            p.setImage(music.getImg());
            p.setImageAlt("Image_" + convertBlanksToUnderline(music.getAlbum_name()));
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_WEEK, 2);
            p.setAuctionEnd(calendar.getTime());
            p.setType(ProductType.ALBUM);
            p.setYear(Integer.parseInt(music.getYear()));
            p.setProducer(music.getArtist());
            p.setExpired(false);
            logger.info("Music: " + p.getName() + " created!");
            serviceFactory.getProductService().createProduct(p);
            p.setRelatedProducts(createDBPedia(p.getProducer(), DBPediaOWL.Album, DBPediaOWL.artist, Locale.GERMAN,p));
        }
        logger.info("Generated musicdata is finished.");

        logger.info("Generated productdata is finished.");
    }


    //returns a List with similar Elements
    private List<RelatedProduct> createDBPedia(String author, Resource typeResource, Resource producerResource, Locale locale, Product product){
        //authorResource: author, director interpreter
        logger.info("Generated relatedproductdata is started.");
        Resource authorResource = DBPediaService.loadStatements(DBPedia.createResource(convertBlanksToUnderline(author)));

        SelectQueryBuilder tempQuery = DBPediaService.createQueryBuilder()
                .setLimit(5)
                .addWhereClause(RDF.type, typeResource)
                .addWhereClause(producerResource, authorResource)
                .addPredicateExistsClause(FOAF.name)
                .addFilterClause(RDFS.label, Locale.ENGLISH)
                .addFilterClause(RDFS.label, Locale.GERMAN);


        Model tempModel = DBPediaService.loadStatements(tempQuery.toQueryString());
        List<String> tempList = DBPediaService.getResourceNames(tempModel, locale);

        List<RelatedProduct> relatedProducts = new ArrayList<>();
        for(String s : tempList){
            RelatedProduct relatedProduct = new RelatedProduct();
            relatedProduct.setName(s);
            relatedProduct.setProduct(product);
            relatedProducts.add(relatedProduct);
            logger.info("Related product: " +relatedProduct.getName() + " created!");
            persist(relatedProduct);
        }
        logger.info("Generated relatedproductdata is finished.");
        return relatedProducts;

    }

    //converts a string e.g. "firstname lastname" to "firstname_lastname"
    private String convertBlanksToUnderline(String stringWithBlanks){
        char[] temp = stringWithBlanks.toCharArray();
        String stringWithUnderline = "";

        for(int i = 0; i < temp.length; i++){
            if(temp[i] == ' '){
                temp[i] = '_';
            }
            stringWithUnderline += temp[i];
        }

        return stringWithUnderline;
    }


    private void persist(Object object) {

        EntityTransaction entityTransaction = entityManager.getTransaction();
        try {
            entityTransaction.begin();
            entityManager.persist(object);
            entityTransaction.commit();
            logger.info("Object saved to database successfully.");
        } catch (RuntimeException e) {
            logger.info("Object saved to Database failed: " + e.getMessage());
            if(entityTransaction.isActive()) {
                entityTransaction.rollback();
                logger.info("Transaction rollback!");
            }
        }
    }
}

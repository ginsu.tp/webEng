package at.ac.tuwien.big.we16.ue2.beans;

import java.util.UUID;

public class UserBean {
	private UUID ID;
    private String email, password;
    private double credit;
    private int runningAuctions,wonAuctions,lostAuctions;

    public UserBean() {
    }
    
    

    /**
     * Bean die beim Anmelden des Users erstellt wird. (abschluss des Logins) 
     * @param email
     * @param password
     * @param credit
     */
    public UserBean(String email, String password, double credit) {
		this.email = email;
		this.password = password;
		this.credit = credit;
		this.setID();
	}



	public UserBean(String email, String password, double credit, int runningAuctions, int wonAuctions, int lostAuctions) {
        this.email = email;
        this.password = password;
        this.credit = credit;
        this.runningAuctions = runningAuctions;
        this.wonAuctions = wonAuctions;
        this.lostAuctions = lostAuctions;
        this.setID();
    }

    public void setID() {
    	ID =  UUID.randomUUID();
    }
    
    public UUID getID() {
    	return ID;
    }
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public int getRunningAuctions() {
        return runningAuctions;
    }

    public void setRunningAuctions(int runningAuctions) {
        this.runningAuctions = runningAuctions;
    }

    public int getWonAuctions() {
        return wonAuctions;
    }

    public void setWonAuctions(int wonAuctions) {
        this.wonAuctions = wonAuctions;
    }

    public int getLostAuctions() {
        return lostAuctions;
    }

    public void setLostAuctions(int lostAuctions) {
        this.lostAuctions = lostAuctions;
    }
    
    /**
     * muss implementiert werden damit der DataStore mit den Beans umgehen kann. 
     */
    public boolean equals(Object that) {
    	if(that == null) {
    		return false;
    	}
    	UserBean usr = (UserBean)that;
    	if(this.ID.equals(usr.getID())) {
    		return true;
    	}
    	else return false;
    }

	@Override
	public String toString() {
		return "UserBean [ID=" + ID + ", email=" + email + ", password=" + password + ", credit=" + credit
				+ ", runningAuctions=" + runningAuctions + ", wonAuctions=" + wonAuctions + ", lostAuctions="
				+ lostAuctions + "]";
	}
    
    
    
}

<%--
  Created by IntelliJ IDEA.
  User: ASUSG74
  Date: 12.04.16
  Time: 23:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="at.ac.tuwien.big.we16.ue2.DataStore" %>
<%@ page import="at.ac.tuwien.big.we16.ue2.beans.*" %>
<%@ page import="java.util.Set" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>

<jsp:useBean id="user" scope="session" class="at.ac.tuwien.big.we16.ue2.beans.UserBean"/>
<% 
ServletContext context = request.getSession().getServletContext();
DataStore store = null;
// sollte nie null sein da nach dem Login der Datastore auf jeden fall initialisiert wird. 
if(context.getAttribute("datastore") == null) {
	store = DataStore.createData();	
} else {
	store = (DataStore)context.getAttribute("datastore");
}
Set<AuctionBean> dataSet = store.getAllAuctions();%>
<!doctype html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>BIG Bid - Produkte</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../styles/style.css">
    <script>
        function initialize(){
            document.getElementById("result").innerHTML = localStorage.clickcount + " clicks";
            /*var test = document.createElement('li').innerText = "test1241241";
            var list = document.getElementById('list').innerHTML = test;

            list.appendChild(test);*/
        }


        function clickCounter() {
            if(typeof(Storage) !== "undefined") {
                if (localStorage.clickcount) {
                    localStorage.clickcount = Number(localStorage.clickcount)+1;
                } else {
                    localStorage.clickcount = 1;
                }
            } else {
                document.getElementById("result").innerHTML = "Sorry, your browser does not support web storage...";
            }
        }
    </script>
</head>
<body data-decimal-separator="," data-grouping-separator="." onload="initialize()">

<a href="#productsheadline" class="accessibility">Zum Inhalt springen</a>

<header aria-labelledby="bannerheadline">
    <img class="title-image" src="../images/big-logo-small.png" alt="BIG Bid logo">

    <h1 class="header-title" id="bannerheadline">
        BIG Bid
    </h1>
    <nav aria-labelledby="navigationheadline">
        <h2 class="accessibility" id="navigationheadline">Navigation</h2>
        <ul class="navigation-list">
            <li>
                <a href="LogoutServlet" class="button" accesskey="l">Abmelden</a>
            </li>
        </ul>
    </nav>
</header>
<div class="main-container">
<aside class="sidebar" aria-labelledby="userinfoheadline">
    <div class="user-info-container">
        <h2 class="accessibility" id="userinfoheadline">Benutzerdaten</h2>
        <dl class="user-data properties">
            <dt class="accessibility">Name:</dt>
            <dd class="user-name" id="user_<%= user.getID()%>"><%=user.getEmail()%></dd>
            <dt>Kontostand:</dt>
            <dd>
                <span class="balance" id="user_balance"><%=user.getCredit()%> €</span>
            </dd>
            <dt>Laufend:</dt>
			<dd>
                <span class="running-auctions-count" id="running_user_<%= user.getID()%>"><%=user.getRunningAuctions()%></span>
                <span class="auction-label" data-plural="Auktionen" data-singular="Auktion">Auktionen</span>
            </dd>
            <dt>Gewonnen:</dt>
            <dd>
                <span class="won-auctions-count" id="won_user_<%= user.getID()%>"><%=user.getWonAuctions()%></span>
                <span class="auction-label" data-plural="Auktionen" data-singular="Auktion">Auktionen</span>
            </dd>
            <dt>Verloren:</dt>
            <dd>
                <span class="lost-auctions-count" id="lost_user_<%= user.getID()%>"><%=user.getLostAuctions()%></span>
                <span class="auction-label" data-plural="Auktionen" data-singular="Auktion">Auktionen</span>
            </dd>
        </dl>
    </div>
    <div class="recently-viewed-container">
        <h3 class="recently-viewed-headline">Zuletzt angesehen</h3>
        <div id="result"> </div>
        <ul id="list" class="recently-viewed-list">
        </ul>
    </div>
</aside>
<main aria-labelledby="productsheadline">
    <h2 class="main-headline" id="productsheadline">Produkte</h2>
    <div class="products">
    
    <%--schleife um alle produkte (aus JSON) zu laden --%>
    
    <% Iterator<AuctionBean> iterator=dataSet.iterator();
    while(iterator.hasNext()){
    AuctionBean auction = iterator.next();%>
    
        <div class="product-outer" data-product-id="<%= auction.getID() %>" onclick="clickCounter()">
        <%
        String inputString ="";
        if(auction != null && auction.getHighestBidder() != null ) {
	        if(user.getID().equals(auction.getHighestBidder().getID())) {
	        	inputString = "highlight";
	        }
        }
        %>
        
            <a href="../ForwardServlet?auctionid=<%= auction.getID().toString()  %>" class="product <%=inputString %>"
               title="Mehr Informationen zu <%= auction.getTitle() %>" id="product_<%=auction.getID() %>" >
                
                <img class="product-image" src="../images/<%= auction.getImg() %>" alt="">
                <dl class="product-properties properties">
                    <dt>Bezeichnung</dt>
                    <dd class="product-name"><%= auction.getTitle() %></dd>
                    <dt>Preis</dt>
                    <dd class="product-price" id="price_<%= auction.getID()%>">
                    <%= auction.getBid()+"€" %>
                        
                    </dd>
                    <dt>Verbleibende Zeit</dt>
                    <% DateFormat dateFormat = new SimpleDateFormat("yyyy,MM,dd,HH,mm,ss,SSS");
                    String auctionEnd = null;
                        auctionEnd = dateFormat.format(auction.getExpireDate());

					 %>
                    <dd data-end-time="<%=auctionEnd %>" data-end-text="abgelaufen"
                        class="product-time js-time-left"></dd>
                    <dt>Höchstbietende/r</dt>
                    
                    <dd class="product-highest" id="highest_bidder_<%= auction.getID() %>">
                    	<%
                    	String highestName = "Noch kein Gebot";
                    	if(auction.getHighestBidder() != null) {
                    			highestName = auction.getHighestBidder().getEmail();
                    		
                    	}
                    	%>
                    	<%= highestName %>
                    </dd>
                </dl>
            </a>
        </div>
        <%} %>
       

    </div>
</main>
</div>
<footer>
    © 2016 BIG Bid
</footer>
<script src="../scripts/jquery.js"></script>
<script src="../scripts/framework.js"></script>
<script src="../scripts/websocket.js"></script>
</body>
</html>
package at.ac.tuwien.big.we16.ue2.beans;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public class AuctionBean {

	private UUID ID;

    private Timestamp expireDate;
    private UserBean highestBidder;
    private boolean isBidden;
    private double bid;
    private String title,producer,year,img;
    private boolean isNotified;
    
    

    public AuctionBean(Timestamp expireDate, String title, String producer, String year, String img) {
		this.expireDate = expireDate;
		this.title = title;
		this.producer = producer;
		this.year = year;
		this.img = img;
		this.setID();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	
    
    public void setID() {
    	ID =  UUID.randomUUID();
    	
    }
    
    public UUID getID() {
    	return ID;
    }



    public Timestamp getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Timestamp expireDate) {
        this.expireDate = expireDate;
    }

    
    /**
     * Berechnung ob die Auction abgelaufen ist. 
     * @return
     */
    public boolean isExpired(){
        if(this.expireDate.before(Timestamp.valueOf(LocalDateTime.now()))){
            return true;
        }
        else {
            return false;
        }
    }
    
    public UserBean getHighestBidder() {
        return highestBidder;
    }

    public void setHighestBidder(UserBean highestBidder) {
        this.highestBidder = highestBidder;
    }

    public boolean isBidden() {
        return isBidden;
    }

    public void setBidden(boolean isBidden) {
        this.isBidden = isBidden;
    }
    
    public double getBid() {
    	return this.bid;
    }
    
    public void setBid(double bid) {
    	this.bid = bid;
    	this.setBidden(true);
    }
    
    /**
     * muss implementiert werden damit der DataStore mit den Beans umgehen kann. 
     */
    public boolean equals(Object that) {
    	if(that == null) {
    		return false;
    	}
    	AuctionBean act = (AuctionBean)that;
    	if(this.ID.equals(act.getID())) {
    		return true;
    	}
    	else return false;
    }

	@Override
	public String toString() {
		return "AuctionBean [ID=" + ID + ", expireDate=" + expireDate + ", highestBidder=" + highestBidder
				+ ", isBidden=" + isBidden + ", bid=" + bid + ", title=" + title + ", producer=" + producer + ", year="
				+ year + ", img=" + img + "]";
	}
	
	public void setNotified() {
		this.isNotified = true;
	}
	
	public boolean getNotfied() {
		return this.isNotified;
	}
	
}

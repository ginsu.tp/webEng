package at.ac.tuwien.big.we16.ue2.service;

import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import at.ac.tuwien.big.we16.ue2.DataStore;
import at.ac.tuwien.big.we16.ue2.Simulation;
import at.ac.tuwien.big.we16.ue2.beans.AuctionBean;
import at.ac.tuwien.big.we16.ue2.beans.UserBean;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class NotifierService {
    private static Map<Session, HttpSession> clients = new ConcurrentHashMap<>();
    private final ScheduledExecutorService executor;
    private final ScheduledExecutorService exec2;

    public NotifierService() {
        // Use the scheduled executor to regularly check for recently expired auctions
        // and send a notification to all relevant users.
        this.executor = Executors.newSingleThreadScheduledExecutor();
        // zuerst alle auctions aus dem Datastore holen
        DataStore data = DataStore.createData();
        
        Set<AuctionBean> auctions = data.getAllAuctions();
        ExpiredRunnable exRun = new ExpiredRunnable(auctions);
        // executor.schedule(exRun, 5, TimeUnit.SECONDS);
        executor.scheduleAtFixedRate(exRun, 0, 5, TimeUnit.SECONDS);
        
        this.exec2 = Executors.newSingleThreadScheduledExecutor();
        // erzeuge einen neuen computerbenutzer der als thread laeuft
        UserBean computerBenutzer = new UserBean("ComputerBenutzer@cp.at", "test", 3000d);
        
        exec2.scheduleAtFixedRate(new Simulation(computerBenutzer, data), 0, 15, TimeUnit.SECONDS);
    }

    /**
     * This method is used by the WebSocket endpoint to save a reference to all
     * connected users. A list of connections is needed so that the users can be
     * notified about events like new bids and expired auctions (see
     * assignment). We need the socket session so that we can push data to the
     * client. We need the HTTP session to find out which user is currently
     * logged in in the browser that opened the socket connection.
     */
    public void register(Session socketSession, HttpSession httpSession) {
        clients.put(socketSession, httpSession);
    }

    public void unregister(Session userSession) {
        clients.remove(userSession);
    }

    /**
     * Call this method from your servlet's shutdown listener to cleanly
     * shutdown the thread when the application stops.
     * 
     * See http://www.deadcoderising.com/execute-code-on-webapp-startup-and-shutdown-using-servletcontextlistener/
     */
    public void stop() {
        this.executor.shutdown();
    }
    
    /**
     * sendet allen Bietern auf ihre Overview Seite die neuesten Daten, wenn ein 
     * neues Gebot gesetzt wird. Die Daten in der Overview werden so aktuell gehalten
     * Preis, hoechter Bieter.
     * @param auction
     */
    public void sendHighestBidToBidders(AuctionBean auction) {
    	Set<Session> bidders = clients.keySet();
    	for(Session b : bidders) {
    		b.getAsyncRemote().sendText("notifyAll;"+auction.getID().toString()+";"
    	+auction.getHighestBidder().getEmail()+";"+auction.getBid()+"�;"+auction.getHighestBidder().getID());
    	}
    }
    
    /**
     * alle Clients, die bei der duch den Parameter mitgegebenen auction mitbieten, 
     * werden informiert dass die auction abgelaufen ist. zus�ztlich werden die Daten
     * in der SideBar aktualisiert(Auktionen gewonnen/verloren/laufend) 
     * @param auction
     */
    public void auctionExpired(AuctionBean auction) {
    	Set<Session> bidders = clients.keySet();
    	for(Session b:bidders) {
    		HttpSession client = clients.get(b);
    		// user herausfinden dem die session gehoert
    		UserBean user = (UserBean)client.getAttribute("user");
    		
    		DataStore store = DataStore.createData();
    		
    		// alle user holen die bei der gegebenen Auktion mitbieten
    		List<UserBean> users =  store.getUsersfromAuction(auction); 
    		for(UserBean u:users) {
    			if(u.equals(user)) {
    				// was wird gesendet ?
    				// expired-code, abgelaufene auktionid, user id, laufende Auktionen, gewonnene, verlorene
    				b.getAsyncRemote().sendText("expired;"+user.getID()+";"+ u.getID()+";"+
    				u.getRunningAuctions()+";"+u.getWonAuctions()+";"+u.getLostAuctions()+";"+u.getCredit());
    			}
    		}
    		
    	}
    }
    
    /**
     * diese methode schreibt einem user sein gebot zurueck wenn er ueberboten wird
     * der alte wert wird durch den neuen wert ersetzt
     * @param user - bekommt credit zurueck
     * @param bid - betrag den er zurueck bekommt 
     */
    public void updatePreHighestBidderCredit(UserBean user) {
    	Set<Session> bidders = clients.keySet();
    	
    	for(Session b: bidders) {
    		HttpSession client = clients.get(b);
    		UserBean aktuell = (UserBean) client.getAttribute("user");
    		
    		if(aktuell.equals(user)) {
    			b.getAsyncRemote().sendText("updateCredit;"+user.getID()+";"+user.getCredit());
    		}
    	}
    }
    
}

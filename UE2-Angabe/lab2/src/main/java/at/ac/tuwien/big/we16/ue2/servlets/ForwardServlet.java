package at.ac.tuwien.big.we16.ue2.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.annotations.SourceType;

import at.ac.tuwien.big.we16.ue2.beans.UserBean;

/**
 * Servlet implementation class ForwardServlet
 */
@WebServlet("/ForwardServlet")
public class ForwardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("auctionid");
        
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/details.jsp?auctionid="+id);
        dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String auctionId = request.getParameter("auctionid");
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/details.jsp?auctionid="+auctionId);
        dispatcher.forward(request, response);
	}

}

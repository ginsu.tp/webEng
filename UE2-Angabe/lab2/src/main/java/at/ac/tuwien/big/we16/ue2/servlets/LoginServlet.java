package at.ac.tuwien.big.we16.ue2.servlets;

import at.ac.tuwien.big.we16.ue2.DataStore;
import at.ac.tuwien.big.we16.ue2.beans.UserBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by ASUSG74 on 20.04.16.
 */

@WebServlet(name="LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        UserBean aktUser = new UserBean(request.getParameter("email"), request.getParameter("password"),1500.0d);
        
        session.setAttribute("user",aktUser);
        
        ServletContext context = request.getSession().getServletContext();
        
        // wenn der datastore noch nicht gesetzt wurde, wird er initialisiert
        if(context.getAttribute("datastore") == null) {
        	DataStore store = DataStore.createData();
        	context.setAttribute("datastore", store );
        	store.setNewUserToDataStore(aktUser);
        }
        else{
        	DataStore store = (DataStore) context.getAttribute("datastore");
        	store.setNewUserToDataStore(aktUser);
        }
        
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/views/overview.jsp");
        // dispatcher.forward(request, response);
        response.sendRedirect("./views/overview.jsp");
    }
}

package at.ac.tuwien.big.we16.ue3.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Bid")
public class Bid implements Serializable{

    @Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
    private String id;
    
	private int amount;
	
	@ManyToOne
	@PrimaryKeyJoinColumn
    private User user;
    
	@ManyToOne
	@PrimaryKeyJoinColumn
	private Product product;

	public Bid(){
		
	}
    public Bid(int centAmount, User user) {
        amount = centAmount;
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public float getConvertedAmount() {
        float convertedAmount = (float)this.amount;
        return convertedAmount / 100;
    }

    public User getUser() {
        return user;
    }

    public boolean isBy(User user) {
        return this.user.equals(user);
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
    public void setAmount(int amount) {
    	this.amount=amount;
    }
}

package at.ac.tuwien.big.we16.ue2;
import java.util.Date;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;
import java.util.Set;
import java.util.Timer;

import at.ac.tuwien.big.we16.ue2.productdata.JSONDataLoader;
import at.ac.tuwien.big.we16.ue2.productdata.JSONDataLoader.*;
import at.ac.tuwien.big.we16.ue2.beans.*;

/**
 * Klasse DataStore als Singleton 
 * Besteht aus 2 Maps:
 * keyUser: um zu speichern welche Auctions ein User hat.
 * keyAuction: speichert welcher User bei einer Auction mitbietet. 
 * 
 * praktisch eine M:N beziehung
 * 
 * zusaetzlich wird das Singleton eine Init Methode aufrufen. Diese laedt initial alle
 * Auctions und erzeugt die dazugehoerigen Beans. 
 * 
 * @author Ginsu George, Max Bischof
 *
 */
public class DataStore {
	
	private static HashMap <UserBean, List<AuctionBean>> keyUser;
	private static HashMap <AuctionBean, List<UserBean>> keyAuction;
	private static DataStore datastore = null;
	
	private DataStore() {

	}
	
	public static synchronized DataStore createData() {
		if(datastore== null) {
			datastore = new DataStore();
			datastore.keyUser = new HashMap<UserBean, List<AuctionBean>>();
			datastore.keyAuction = new HashMap<AuctionBean, List<UserBean>>();
			DataStore.initialDataLoad(); // intial load der Daten aus dem JSONDataLoader
		}
		return datastore;
	}
	
	/**
	 * wird nur beim Erstellen des singletons initial aufgerufen
	 */
	private static void initialDataLoad() {
		Book[] book=JSONDataLoader.getBooks();
		Music[] music=JSONDataLoader.getMusic();
		Movie[] movie=JSONDataLoader.getFilms();
		Date date=new Date();
		long now= date.getTime();
		Timestamp then;
		Random rand = new Random();
		int range=3600;
		
		//beans erstellen
		for(Book b:book){
			then = new Timestamp(now+rand.nextInt(range)*1000);
			datastore.keyAuction.put(new AuctionBean(then,b.getTitle(),b.getAuthor(),b.getYear(),b.getImg()), new LinkedList<UserBean>());
		}
		for(Music b:music){
			then = new Timestamp(now+rand.nextInt(range)*1000);
			datastore.keyAuction.put(new AuctionBean(then,b.getAlbum_name(),b.getArtist(),b.getYear(),b.getImg()), new LinkedList<UserBean>());
		}
		for(Movie b:movie){
			then = new Timestamp(now+rand.nextInt(range)*1000);
			datastore.keyAuction.put(new AuctionBean(then,b.getTitle(),b.getDirector(),b.getYear(),b.getImg()), new LinkedList<UserBean>());
		}
		
		// ids beim initial
		
	}
	
	/**
	 * legt einen neuen User im DataStore an, welcher noch keine Auctions hat.
	 * Wenn es den User bereits gibt, wird nichts gemacht
	 * 
	 * @param user
	 */
	public void setNewUserToDataStore(UserBean user) {
		if(datastore.keyUser.keySet().contains(user)) {
			return;
		}
		else 
			datastore.keyUser.put(user, new LinkedList<AuctionBean>());
	}
	
	/**
	 * Wenn ein User bei einer Auction mitbietet, wird diese Methode aufgerufen. 
	 * Diese speichert den user in die Liste der Mitbietenden. 
	 * @param user 
	 * @param auction key der Map.
	 */
	public void setUserToAuctionAsBidder(UserBean user, AuctionBean auction) {
		if(datastore.keyAuction.keySet().contains(auction)) {
			List<UserBean> l = this.keyAuction.get(auction);
			l.add(user);
			datastore.keyAuction.replace(auction, l);
		}
	}
	
	/**
	 * Methode teilt einem bestehendem User eine neue Auction zu. 
	 * Zu dem UserBean user kommt die auction dazu.
	 * 
	 * Beide Werte duerfen nicht null sein. 
	 * Wenn der User nicht im DataStore enthalten ist, wird er neu angelegt.
	 */
	public void setAuctionToUser(UserBean user, AuctionBean auction) {
		// wenn der user schon existiert 
		if(datastore.keyUser.keySet().contains(user)) {
			List l = keyUser.get(user);
			l.add(auction);
			datastore.keyUser.replace(user, l);
		} else { // user existiert noch nicht und wird neu eingefuegt
			LinkedList<AuctionBean> l = new LinkedList<>();
			l.add(auction);
			datastore.keyUser.put(user, l);
		}
	}
	
	
	/**
	 * erstellt ein Set aus allen existierenden Auctions
	 */
	public Set<AuctionBean> getAllAuctions(){
		return datastore.keyAuction.keySet();
	}
	
	/**
	   * diese Methode retouniert eine AuctionBean, welche eine ID besitzt, die als Parameter uebergeben wird.
	   * Falls es so eine Bean nicht gibt wird null ausgegeben. 
	   * @param id
	   * @return
	   */
	  public AuctionBean getAuctionByID(String id) {
		//System.out.println("anzahl der daten: " + datastore.keyAuction.size());
	    //System.out.println("gesuchte: " + id);
		Set auctionen = datastore.keyAuction.keySet();
	    Iterator it2 = auctionen.iterator();
	    
	    ArrayList<AuctionBean> liste = new ArrayList<AuctionBean>(auctionen);
	   
	    for(AuctionBean a:liste) {
	      //System.out.println(a.getID());
	      if(a.getID().toString().equals(id)) {
	        return a;
	      }
	    }
	    return null;
	  }
	  
	  /**
	   * retourniert eine Liste von Auctionen, bei denen der User, welcher 
	   * als Parameter mitgegeben wird, mitbietet
	   * @param user
	   * @return
	   */
	  public List<AuctionBean> getAuctionFromUser(UserBean user) {
	    return this.keyUser.get(user);
	  }
	  
	  public List<UserBean> getUsersfromAuction(AuctionBean auction){
		  return this.keyAuction.get(auction);
	  }
	  


}

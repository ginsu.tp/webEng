package at.ac.tuwien.big.we16.ue2.servlets;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import at.ac.tuwien.big.we16.ue2.DataStore;
import at.ac.tuwien.big.we16.ue2.beans.AuctionBean;
import at.ac.tuwien.big.we16.ue2.beans.UserBean;
import at.ac.tuwien.big.we16.ue2.service.NotifierService;
import at.ac.tuwien.big.we16.ue2.service.ServiceFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class BidServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        double bid = Double.parseDouble(req.getParameter("newprice"));
        String productID = req.getParameter("productID");
        
        ServletContext context = req.getSession().getServletContext();
        DataStore data = null;
        // sollte nie null sein da nach dem Login der Datastore auf jeden fall initialisiert wird. 
        if(context.getAttribute("datastore") == null) {
        	data = DataStore.createData();	
        } else {
        	data = (DataStore)context.getAttribute("datastore");
        }
        
    	// notifierservice aus der factory createn
    	NotifierService service = ServiceFactory.getNotifierService();
        
        AuctionBean currentAuction=data.getAuctionByID(productID);
        
        // der vorherige hoechtbieter
        UserBean preHighestUser = currentAuction.getHighestBidder();
        double preBid = currentAuction.getBid(); // vorheriges hoechstgebot
        

        
        UserBean currentUser=(UserBean) req.getSession().getAttribute("user");
        resp.setContentType("text/html");
      
        // dem alten hoechstbietenden muss sein gebot wieder zurueck ueberwiesen werden
        // falls es einen gab bzw er sich nicht selber ueberbietet
        if(preHighestUser != null && preHighestUser != currentUser) {
        	preHighestUser.setCredit(preHighestUser.getCredit()+preBid);
        	// aufruf zum service zum websocket
        	service.updatePreHighestBidderCredit(preHighestUser); 
        }
        
        if(bid<=currentAuction.getBid()||bid>currentUser.getCredit()){
        	resp.getWriter().write("false");	
        }
        else{
        	//liste der Mitbietenden aktualisieren (bzw erstellen wenn es noch keine gebote gibt)
        	List<UserBean> userList = data.getUsersfromAuction(currentAuction);
        	if(!userList.contains(currentUser)){
        		userList.add(currentUser);
        	}
        	
        	

        	//liste der auktionen fuer aktuellen user aktualisieren
        	List<AuctionBean> auctionList= data.getAuctionFromUser(currentUser);
        	if(!auctionList.contains(currentAuction)){
        		auctionList.add(currentAuction);
        		currentUser.setRunningAuctions(currentUser.getRunningAuctions()+1);
        	}        	
        	
        	//usercredit aktualisieren (wenn er bereits hoechstbietender ist, wird nur die differenz zu altem gebot abgezogen
        	if(currentAuction.getHighestBidder() !=null && currentAuction.getHighestBidder().equals(currentUser)){
        	currentUser.setCredit(currentUser.getCredit()-bid+currentAuction.getBid());
        	}
        	else{
        	currentUser.setCredit(currentUser.getCredit()-bid);
        	}
        	
        	//Hoechstbietenden aktualisieren
        	currentAuction.setHighestBidder(currentUser);
        	//Hoechstgebot aktualisieren
        	currentAuction.setBid(bid);
        	//Email des hoechstbietenden ; kontostand des aktuellen Users ; auktionen an denen der User beteiligt ist ; hoechstgebot
        	resp.getWriter().write(currentUser.getEmail()+";"+currentUser.getCredit()+";"+auctionList.size()+";"+bid);
        	
        	
        	//
        	// alle anderen bieter benachrichtigen und ihre overview page aktualisieren
        	//
        	service.sendHighestBidToBidders(currentAuction);
        	
        	
        	
    
        }
        
        
        

    }
}

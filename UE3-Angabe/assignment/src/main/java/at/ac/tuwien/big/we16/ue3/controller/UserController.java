package at.ac.tuwien.big.we16.ue3.controller;

import at.ac.tuwien.big.we16.ue3.model.User;
import at.ac.tuwien.big.we16.ue3.service.AuthService;
import at.ac.tuwien.big.we16.ue3.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.concurrent.TimeUnit;

public class UserController {
    private final UserService userService;
    private final AuthService authService;

    public UserController(UserService userService, AuthService authService) {
        this.userService = userService;
        this.authService = authService;
    }

    public void getRegister(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (this.authService.isLoggedIn(request.getSession())) {
            response.sendRedirect("/");
            return;
        }
        request.getRequestDispatcher("/views/registration.jsp").forward(request, response);
    }

    public void postRegister(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        User user = new User();
        boolean validUserInput=true;
        
        String salutation = request.getParameter("salutation");
        System.out.println(salutation);
        if(salutation.equals("mr")|| salutation.equals("ms")){
        	user.setSalutation(salutation);
        }
        else {
        	request.setAttribute("salutation", "false");
        	validUserInput=false;
        }
        
        
		String firstname = request.getParameter("firstname");
		if(!firstname.equals("")){
			user.setFirstname(firstname);
		}
		else {
			request.setAttribute("firstname", "false");
			validUserInput=false;
		}
		
		
		String lastname = request.getParameter("lastname");
		if(!lastname.equals("")){
			user.setLastname(lastname);
		}
		else {
			request.setAttribute("lastname", "false");
			validUserInput=false;
		}
		
		
		String dateofbirth = request.getParameter("dateofbirth");
		if(isValidDate(dateofbirth)){
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
  	      try {
			Date d = sdf.parse(dateofbirth);
			user.setDate(d);
		} catch (ParseException e) {
			//already handeled inisValidDate()
		}
		}
		else{
			request.setAttribute("dateofbirth", "false");
			validUserInput=false;
		}
		
		
		String email = request.getParameter("email");
		//if(email.matches("/^\S+@\S+\.\S+$/")){ (regex aus der angabe funktioniert nicht)
		if(!email.equals("")){
			user.setEmail(email);
		}
		else{
			request.setAttribute("email", "false");
			validUserInput=false;
		}
		
		
		String password = request.getParameter("password");
		if(password.length()>=4 && password.length()<=8){
			user.setPassword(password);
		}
		else{
			request.setAttribute("password", "false");
			validUserInput=false;
		}
		
		/*
		String streetAndNumber = request.getParameter("streetAndNumber");
		String postcodeAndCity = request.getParameter("postcodeAndCity");
		String country = request.getParameter("country");
		*/
        if(validUserInput==true){
        this.userService.createUser(user);
        this.authService.login(request.getSession(), user);
        response.sendRedirect("/we16-ue3/");
        }
        
        else{
        request.setAttribute("user", user);        
        request.getRequestDispatcher("/views/registration.jsp").forward(request, response);
        }
    }
    
    private boolean isValidDate(String date){
    	try {
    	      SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
    	      Date d = sdf.parse(date);
    	      Date now=new Date();
    	      long diff= getDateDiff(d,now,TimeUnit.DAYS);
    	      System.out.println(diff);
    	      if(diff<365*18){
    	    	  return false;
    	      }
    	      return true;
    	      
    	} catch (ParseException pe) {
    	      return false;
    	}
    }
    
    
    public long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

}

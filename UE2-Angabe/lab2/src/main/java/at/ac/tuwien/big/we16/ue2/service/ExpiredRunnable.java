package at.ac.tuwien.big.we16.ue2.service;

import java.util.List;
import java.util.Set;

import at.ac.tuwien.big.we16.ue2.DataStore;
import at.ac.tuwien.big.we16.ue2.beans.AuctionBean;
import at.ac.tuwien.big.we16.ue2.beans.UserBean;

public class ExpiredRunnable implements Runnable {

	private Set<AuctionBean> auctions;
	
	public ExpiredRunnable(Set<AuctionBean> auctions) {
		this.auctions = auctions;
	}
	
	@Override
	public void run() {

		for(AuctionBean a:auctions) {
			// wenn auction abgelaufen ist
			if(a.isExpired() && a.getNotfied() != true) {
				UserBean highestBidder = a.getHighestBidder();
				
				// alle laufenden um -1 und verlorene um +1, bei denen die mitbieten
				// nur beim hoechstbieter wird statt verloren +1 gewonnen +1 gesetzt
				DataStore store = DataStore.createData();
				List<UserBean> aktUser = store.getUsersfromAuction(a);
				for(UserBean user:aktUser ) {
					user.setRunningAuctions(user.getRunningAuctions()-1);
					if(user.equals(highestBidder)) {
						user.setWonAuctions(user.getWonAuctions()+1);
					} else {
						user.setLostAuctions(user.getLostAuctions()+1);
					}
					
				}		
				a.setNotified();	
				// abgelaufene auction an den notifier senden damit alle die benachrichtung kriegen
				NotifierService ns = ServiceFactory.getNotifierService();
				ns.auctionExpired(a);		
			}
		}	
	}
}

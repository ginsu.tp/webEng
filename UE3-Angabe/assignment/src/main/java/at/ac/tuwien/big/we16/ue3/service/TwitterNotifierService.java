package at.ac.tuwien.big.we16.ue3.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import at.ac.tuwien.big.we16.ue3.exception.RequestException;
import at.ac.tuwien.big.we16.ue3.model.Bid;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
/**
 * diese klasse benachrichtigt ecosio und twitter bei einem bid.
 * @author Ginsu George
 *
 */
public class TwitterNotifierService {
	
	private static Twitter twitter;
	private static Set<String> sentBids = new HashSet<>();
	
	// daten aus der angabe an das Twitter object uebergeben 
	static {
		TwitterFactory factory = new TwitterFactory();
		twitter = factory.getInstance();
		// set consumerKey and consumerSecret
		twitter.setOAuthConsumer("GZ6tiy1XyB9W0P4xEJudQ", "gaJDlW0vf7en46JwHAOkZsTHvtAiZ3QUd2mD1x26J9w");
		//set accesstoken and tokensecret 
		AccessToken at = new AccessToken("1366513208-MutXEbBMAVOwrbFmZtj1r4Ih2vcoHGHE2207002", 
				"RMPWOePlus3xtURWRVnv1TgrjTyK7Zk33evp4KKyA");
		twitter.setOAuthAccessToken(at);
	}

	/**
	 * methode wird aufgerufen wenn eine auktion ablaueft und diese
	 * auch geboten wurde. der endpoint nimmt nur requests im folgenden format:
	 * {
	 * "name": "John Doe",
	 * "product": "The Catcher in the Rye",
	 * "price": "22.30",
	 * "date": "2016-03-06T11:09:31.701Z"
	 * }
	 * @param bid
	 * 
	 */
	public static void inform(Bid bid)  {
		
		if(sentBids.contains(bid.getProduct().getId())) {
			return; // wurde schon gesendet
		}
		else {
			try {
				URL ecosio = new URL("https://lectures.ecosio.com/b3a/api/v1/bids");
				StringBuilder builder = new StringBuilder();
				builder.append("{");
				builder.append("\"name\": \"" + bid.getUser().getFullName() +"\",");
				builder.append("\"product\": \"" + bid.getProduct().getName()+"\",");
				builder.append("\"price\": \""+bid.getAmount() + "\",");
				builder.append("\"date\": \"" + bid.getProduct().getAuctionEnd().toString()+"\"");
				builder.append("}");
				
				HttpsURLConnection conection = (HttpsURLConnection) ecosio.openConnection();
				conection.setDoOutput(true);
				conection.setRequestMethod("POST");
				conection.setRequestProperty("Content-type", "application/json");
		        
		        OutputStreamWriter out = new OutputStreamWriter(conection.getOutputStream());
		        out.write(builder.toString());
		        out.flush();

		        // holen des respond code und checken ob der request richtig gesendet wurde
		        if(conection.getResponseCode() == 200) {
		            BufferedReader in = new BufferedReader(new InputStreamReader(conection.getInputStream()));
		            String inputLine;
		            StringBuilder msg = new StringBuilder();

		            while ((inputLine = in.readLine()) != null) {
		                msg.append(inputLine);
		            }
		            System.out.println("Status geupdatet");
		            in.close();

		            // in msg steht die antwort von ecosio drinn, also die daten die hingeschickt wurden
		            // + einer UUID... diese wird ausgelesen und an twitter gesendet.
		            twitter.updateStatus(msg.toString());
		            
		        } else {
		        throw new IOException("FEHLERCODE: "+conection.getResponseCode());
		        }
			} catch (MalformedURLException e) {
				// wenn ecosio nicht erreichbar ist. 
				System.out.println("URL is malformed: " + e.getMessage());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TwitterException e) {
				System.out.println("Statusupdate auf Twitter funktioniert nicht!");
				e.printStackTrace();
			}
		}
		
	}
}

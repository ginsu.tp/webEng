package at.ac.tuwien.big.we16.ue3.service;

import at.ac.tuwien.big.we16.ue3.exception.UserNotFoundException;
import at.ac.tuwien.big.we16.ue3.model.User;
import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.HibernateError;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

public class UserService {
	
	private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	

	/**
	 * transaction muss durchgefuehrt werden weil sonst ein timeout
	 * kommt wegen der sperre der user tabelle 
	 * @param user
	 */
    public void createUser(User user) {
    	Session session = sessionFactory.openSession();
    	Transaction t = null;
    	String userId = null;
    	try {
    		t = session.beginTransaction();
    		userId = (String)session.save(user);
    		user.setId(userId);
    		t.commit();
        	
    	}catch(HibernateException e ) {
    		System.out.println("Speichern des Users funktioniert nicht");
    	}
    	finally {
    	 	session.flush();
    		session.close();
    	}
    	
    }

    /**
     * 
     * @param email
     * @return
     * @throws UserNotFoundException
     */
    public User getUserByEmail(String email) throws UserNotFoundException {
    	Session session = sessionFactory.openSession();
    	User user = new User();
    	Criteria criteria = session.createCriteria(User.class);
    	 user = (User) criteria.add(Restrictions.eq("email", email)).uniqueResult();
        return user;
    }


}

package at.ac.tuwien.big.we16.ue3.service;

import at.ac.tuwien.big.we16.ue3.exception.ProductNotFoundException;
import at.ac.tuwien.big.we16.ue3.model.Bid;
import at.ac.tuwien.big.we16.ue3.model.Product;
import at.ac.tuwien.big.we16.ue3.model.User;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProductService {
	private SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
	public void createProduct(Product product) {
    	Session session = sessionFactory.openSession();
    	Transaction t = null;
    	String productId = null;
    	try {
    		t = session.beginTransaction();
    		productId = (String)session.save(product);
    		product.setId(productId);
    		t.commit();
        	
    	}catch(HibernateException e ) {
    		System.out.println("Speichern des Users funktioniert nicht");
    	}
    	finally {
    	 	session.flush();
    		session.close();
    	}
    	
    }
	
    public Collection<Product> getAllProducts() {
    	Session session = sessionFactory.openSession();
    	List<Product> list = session.createCriteria(Product.class).list();
        return list;
    }

    public Product getProductById(String id) throws ProductNotFoundException {

    	Session session = sessionFactory.openSession();
    	Criteria criteria = session.createCriteria(Product.class);
    	 Product product = (Product) criteria.add(Restrictions.eq("ID", id)).uniqueResult();
        return product;
    }

    //TODO: write changed users and products to db
    public Collection<Product> checkProductsForExpiration() {
        Collection<Product> newlyExpiredProducts = new ArrayList<>();
        for (Product product : this.getAllProducts()) {
            if (!product.hasExpired() && product.hasAuctionEnded()) {
                product.setExpired();
                newlyExpiredProducts.add(product);
                if (product.hasBids()) {
                    Bid highestBid = product.getHighestBid();
                    for (User user : product.getUsers()) {
                        user.decrementRunningAuctions();
                        if (highestBid.isBy(user)) {
                            user.incrementWonAuctionsCount();
                        }
                        else {
                            user.incrementLostAuctionsCount();
                        }
                    }
                }
            }
        }
        return newlyExpiredProducts;
    }
}

<%--
  Created by IntelliJ IDEA.
  User: ASUSG74
  Date: 12.04.16
  Time: 23:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ page import="at.ac.tuwien.big.we16.ue2.DataStore" %>
<%@ page import="at.ac.tuwien.big.we16.ue2.beans.*" %>
<%@ page import="java.util.UUID" %>
<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.ParseException" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>



<jsp:useBean id="user" scope="session" class="at.ac.tuwien.big.we16.ue2.beans.UserBean"/>

<html>
<!doctype html>
<html lang="de">
    <% 
    
    ServletContext context = request.getSession().getServletContext();
    DataStore data = null;
    // sollte nie null sein da nach dem Login der Datastore auf jeden fall initialisiert wird. 
    if(context.getAttribute("datastore") == null) {
    	data = DataStore.createData();	
    } else {
    	data = (DataStore)context.getAttribute("datastore");
    }
    
    String aid = request.getParameter("auctionid");
    // System.out.println(aid+"in details");
    AuctionBean auction = null;
    if(!(aid.equals("") || aid == null)) {
        auction = data.getAuctionByID(aid);
      } 
      
      if(auction == null) {
        auction = new AuctionBean(null, "ID ist nicht mehr gültig !", "", "", "");
      }
    
    // System.out.println(auction.toString());
    
    DateFormat dateFormat = new SimpleDateFormat("yyyy,MM,dd,HH,mm,ss,SSS");
                    String auctionEnd = null;
                        auctionEnd = dateFormat.format(auction.getExpireDate());

					 
%>

<head>
    <meta charset="utf-8">
    <title><%= auction.getTitle() %></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./styles/style.css">
    <script>
        function initialize(){
            document.getElementById("result").innerHTML = localStorage.clickcount + " clicks";
            /*
             var test = document.createElement('li').innerText = "test1241241";
             var list = document.getElementById('list').innerHTML = test;

             list.appendChild(test);*/
        }
    </script>
</head>
<body data-decimal-separator="," data-grouping-separator="." onload="initialize()">

<a href="#productsheadline" class="accessibility">Zum Inhalt springen</a>

<header aria-labelledby="bannerheadline">
    <img class="title-image" src="../images/big-logo-small.png" alt="BIG Bid logo">

    <h1 class="header-title" id="bannerheadline">
        BIG Bid
    </h1>
    <nav aria-labelledby="navigationheadline">
        <h2 class="accessibility" id="navigationheadline">Navigation</h2>
        <ul class="navigation-list">
            <li>
                <a href="LogoutServlet" class="button" accesskey="l">Abmelden</a>
            </li>
        </ul>
    </nav>
</header>
<div class="main-container">
    <aside class="sidebar" aria-labelledby="userinfoheadline">
        <div class="user-info-container">
            <h2 class="accessibility" id="userinfoheadline">Benutzerdaten</h2>
            <dl class="user-data properties">
                <dt class="accessibility">Name:</dt>
                <dd class="user-name"><%= user.getEmail() %></dd>
                <dt>Kontostand:</dt>
                <dd>
                    <span class="balance"><%=user.getCredit() %> €</span>
                </dd>
                <dt>Laufend:</dt>
                <dd>
                    <span class="running-auctions-count"> <%=user.getRunningAuctions() %></span>
                    <span class="auction-label" data-plural="Auktionen" data-singular="Auktion">Auktionen</span>
                </dd>
                <dt>Gewonnen:</dt>
                <dd>
                    <span class="won-auctions-count"><%=user.getWonAuctions()%></span>
                    <span class="auction-label" data-plural="Auktionen" data-singular="Auktion">Auktionen</span>
                </dd>
                <dt>Verloren:</dt>
                <dd>
                    <span class="lost-auctions-count"><%=user.getLostAuctions() %></span>
                    <span class="auction-label" data-plural="Auktionen" data-singular="Auktion">Auktionen</span>
                </dd>
            </dl>
        </div>
        <div class="recently-viewed-container">
            <h3 class="recently-viewed-headline">Zuletzt angesehen</h3>
            <div id="result"> </div>
            <ul id="list" class="recently-viewed-list"></ul>
        </div>
    </aside>
    <main aria-labelledby="productheadline" class="details-container">
        <div class="details-image-container">
            <img class="details-image" src="<%= "./images/"+auction.getImg() %>" alt="">
        </div>
        <div data-product-id="ce510a73-408f-489c-87f9-94817d845773" class="details-data">
            <h2 class="main-headline" id="productheadline"><%= auction.getTitle() %></h2>

            <div class="auction-expired-text" style="display:none">
                <p>
                    Diese Auktion ist bereits abgelaufen.
                    Das Produkt wurde um
                    <span class="highest-bid"> <%= auction.getBid() %> </span> an
                    <%
                    String highestBidder = "";
                    if(auction.getHighestBidder() != null) {
                    	highestBidder = auction.getHighestBidder().getEmail();
                    }
                    %>
                    <span class="highest-bidder"><%= highestBidder %></span> verkauft.
                </p>
            </div>
            <p class="detail-time">Restzeit: <span data-end-time="<%=auctionEnd %>"
                                                   class="detail-rest-time js-time-left"></span>
            </p>
            <form action="./views/overview.jsp">
            <input type="submit"  class="bid-form-field-button" name="back-to-overview" value="Zurück">
            </form>
            <form id="AjaxBidValidation" class="bid-form" method="post" action="">
                <label class="bid-form-field" id="highest-price">
                <% String amount ="Noch kein Gebot";
                if (auction.getBid()>0.0){
                	amount = auction.getBid()+"";
                }%>
                    <span class="highest-bid"><%=amount %></span>
                    <span class="highest-bidder"> <%= highestBidder %> </span>
                </label>
                <label class="accessibility" for="new-price"></label>
                <input type="number" step="0.01" min="0" id="new-price" class="bid-form-field form-input"
                       name="new-price" required>
                       <input type="hidden" id="productID" value="<%= aid %>">
                <p class="bid-error">Es gibt bereits ein höheres Gebot oder der Kontostand ist zu niedrig.</p>
                <input type="button" id="submit-price" class="bid-form-field button" name="submit-price" value="Bieten">
            </form>
        </div>
    </main>
</div>
<footer>
    © 2016 BIG Bid
</footer>
<script src="./scripts/jquery.js"></script>
<script src="./scripts/framework.js"></script>
<script src="./scripts/ajax.js"></script>
<script src="./scripts/websocket.js"></script>
</body>
</html>

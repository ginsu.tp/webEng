package at.ac.tuwien.big.we16.ue3.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "RelatedProduct")
public class RelatedProduct implements Serializable{

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private String id;
    private String name;
    
    @ManyToOne
    private Product product;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
    	this.name =name;
    }
    
    public String getName() {
        return name;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }


}

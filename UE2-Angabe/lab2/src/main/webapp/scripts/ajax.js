/**
 * ajax eventhandler -> wird bei klick auf bieten button aufgerufen und 
 * sendet ajax request an das BidServlet. dort wird das gebot validiert
 *  und eine response zurueckgeschickt
 */

$(document).ready(function() {
        $('#submit-price').click(function(event) {
                var price = $('#new-price').val();
                var id = $('#productID').val();
                $.get('BidServlet', {
                        newprice : price,
                        productID : id                        
                }, function(responseText) {
                	if(responseText=="false"){
                		$('.bid-error').css("display","block");
                	}
                	else{
                //Email des hoechstbietenden ; kontostand des aktuellen Users ; auktionen an denen der User beteiligt ist ; hoechstgebot
                	var params = responseText.split(";");
                	$('.highest-bidder').text(params[0]);
                	$('.balance').text(params[1]);
                	$('.running-auctions-count').text(params[2]);
                	$('.highest-bid').text(params[3]);
                	$('.bid-error').css("display","none");
                	
                	}
                       
                });
                
        });
});